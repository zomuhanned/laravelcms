@extends('layouts.app')

@section('content')

<div class="container">

    @if(session('status'))
        <div class="alert alert-info">{{ session('status') }}</div>
    @endif

    <a href="{{ route('pages.create') }}" class="btn btn-default">Create New Page</a>
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>URL</th>
                <th>Content</th>
                <th>Creator</th>
                <th>Actions</th>
            </tr>
        </thead>

        @foreach ($pages as $page)
            <tr>
                <td>{{ $page->id }}</td>
                <td>
                    <a href="{{ route('pages.edit',['page' => $page->id]) }}">{{ $page->title }}</a>
                </td>
                <td>{{ $page->url }}</td>
                <td>{{ $page->content }}</td>
                <td>{{ $page->user()->first()->name }}</td>
                <td>
                    <form action="{{ route('pages.destroy', ['page' => $page->id]) }}" method="POST">
                        @csrf

                        @method('DELETE')

                        <button type="submit" class="btn btn-danger btn-block">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach

    </table>
    {{ $pages->links() }}
</div>

@endsection