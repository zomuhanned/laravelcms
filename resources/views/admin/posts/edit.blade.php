@extends('layouts.app')

@section('content')

<div class="container">
    <h1>Edit Post</h1>

    <form action="{{ route('posts.update', ['post' => $model->id]) }}" method="post">
        {{ method_field('PUT') }}
        @include('admin.posts.partials.fields')
    
    </form>
    
</div>

@endsection