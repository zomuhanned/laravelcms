{!! csrf_field() !!}


@if (!$errors->isEmpty())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $message)
            
                <li>{{ $message }}</li>

            @endforeach
        </ul>
    </div>
@endif
<div class="form-group">
    <label for="title">Title</label>
    <input type="text" class="form-control" id="title" name="title" value="{{ $model->title }}">
</div>
<div class="form-group">
    <label for="slug">slug</label>
    <input type="text" class="form-control" id="slug" name="slug" value="{{ $model->slug }}">
</div>
<div class="form-group">
    <label for="excerpt">Excerpt</label>
    <textarea name="excerpt" id="excerpt" class="form-control">{{ $model->excerpt }}</textarea>
</div>
<div class="form-group">
    <label for="Body">Body</label>
    <textarea name="body" id="body" class="form-control">{{ $model->body }}</textarea>
</div>
<div class="form-group">
    <input type="submit" class="btn btn-default" value="Submit" />
</div>