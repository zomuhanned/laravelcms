@extends('layouts.app')

@section('content')

<div class="container">
    <h1>Create New Post</h1>

    <form action="{{ route('posts.store') }}" method="post">
        @include('admin.posts.partials.fields')
    </form>
    
</div>

@endsection