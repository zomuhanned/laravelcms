@extends('layouts.app')

@section('content')

<div class="container">

    @if(session('status'))
        <div class="alert alert-info">{{ session('status') }}</div>
    @endif

    <a href="{{ route('blog.create') }}" class="btn btn-default">Create New Post</a>
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Slug</th>
                <th>Author</th>
                <th>Published</th>
                <th>Actions</th>
            </tr>
        </thead>

        @foreach ($posts as $post)
            <tr>
                <td>{{ $post->id }}</td>
                <td>
                    <a href="{{ route('blog.edit',['blog' => $post->id]) }}">{{ $post->title }}</a>
                </td>
                <td>{{ $post->slug }}</td>
                <td>{{ $post->user()->first()->name }}</td>
                <td></td>
                <td>
                    <form action="{{ route('blog.destroy', ['blog' => $post->id]) }}" method="POST">
                        @csrf

                        @method('DELETE')

                        <button type="submit" class="btn btn-danger btn-block">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach

    </table>
    {{ $posts->links() }}
</div>

@endsection