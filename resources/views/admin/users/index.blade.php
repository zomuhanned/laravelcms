@extends('layouts.app')

@section('content')

<div class="container">

    @if(session('status'))
        <div class="alert alert-info">{{ session('status') }}</div>
    @endif
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Roles</th>
                <th>Actions</th>
            </tr>
        </thead>

        @foreach ($model as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>
                    <a href="{{ route('users.edit',['user' => $user->id]) }}">{{ $user->name }}</a>
                </td>
                <td>{{ $user->email }}</td>
                <td>
                    {{ implode(', ', $user->roles()->get()->pluck('name')->toArray()) }}
                </td>
                <td>
                    <form action="{{ route('users.destroy', ['user' => $user->id]) }}" method="POST">
                        @csrf

                        @method('DELETE')

                        <button type="submit" class="btn btn-danger btn-block">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach

    </table>
    {{ $model->links() }}
</div>

@endsection