@extends('layouts.app')

@section('content')

<div class="container">
    <h1>Edit {{ $model->name }}</h1>

    <form action="{{ route('users.update', ['user' => $model->id]) }}" method="post">
        {{ method_field('PUT') }}
        {!! csrf_field() !!}

        @if (!$errors->isEmpty())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $message)
                    
                        <li>{{ $message }}</li>

                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ $model->name }}">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" id="email" name="email" value="{{ $model->email }}">
        </div>
        @foreach($roles as $role)
        <div class="checkbox">
            <label>
                <input type="checkbox" name="roles[]" value="{{ $role->id }}"
                {{ $model->hasRole($role->name) ? 'checked' : '' }} />
                {{ $role->name }}
            </label>
        </div>
        @endforeach
        <div class="form-group">
            <input type="submit" class="btn btn-default" value="Submit" />
        </div>
    
    </form>
    
</div>

@endsection