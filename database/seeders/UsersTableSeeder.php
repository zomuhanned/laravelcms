<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $admin = Role::where('name', 'admin')->first();
        $author = Role::where('name', 'author')->first();

        User::truncate();

        $adminUser = User::create([
                "name" => "admin",
                "email" => "admin@admin.com",
                "password" => bcrypt("password")
        ]);

        $authorUser = User::create([
                "name" => "mhnd",
                "email" => "mhnd@gmail.com",
                "password" => bcrypt("password")
        ]);

        $adminUser->roles()->attach($admin);
        $authorUser->roles()->attach($author);

    }
}
