<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Post;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $admin = User::find(1);

        Post::truncate();

        $admin->posts()->saveMany([
            new Post([
                'title' => 'First Blog',
                'slug' => '/first-blog',
                'excerpt' => 'First blog excerpt',
                'body' => 'This is body of first blog'
            ]),
            new Post([
                'title' => 'Second Blog',
                'slug' => '/second-blog',
                'excerpt' => 'Second blog excerpt',
                'body' => 'This is body of second blog'
            ]),
            new Post([
                'title' => 'Third Blog',
                'slug' => '/third-blog',
                'excerpt' => 'Third blog excerpt',
                'body' => 'This is body of third blog'
            ]),

        ]);
    }
}
