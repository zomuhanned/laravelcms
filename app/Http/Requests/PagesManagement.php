<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PagesManagement extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            //
            'title' => 'required|string|max:25',
            'url' => 'required|string',
            'content' => 'required|string|max:500'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'You must fill the title!',
            'url.required' => 'You must fill the url!',
            'content.required' => 'Please enter some content!'
        ];
    }
}
