<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use App\Models\Role;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    public function __construct(){
        $this->middleware('admin');
        $this->middleware('can:manageUsers,App\Models\User');
    }    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.users.index')->with('model', User::paginate(1));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
        if(Auth::user()->id == $user->id){
            return redirect()->route('users.index')->with('status', 'You cannot edit yourself!');
        }
        return view('admin.users.edit', [
            'model' => $user,
            'roles' => Role::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
        if(Auth::user()->id == $user->id){
            return redirect()->route('users.index')->with('status', 'You cannot edit yourself!');
        }
        
        $user->roles()->sync($request->roles);
        $user->fill($request->only('name', 'email'));
        $user->save();

        return redirect()->route('users.index')->with('status', "$user->name updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
        if(Auth::user()->id == $user->id){
            return redirect()->route('users.index')->with('status', 'You cannot delete yourself!');
        }

        $user->delete();
        return redirect()->route('users.index');
    }
}
