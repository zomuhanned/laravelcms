<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Requests\PagesManagement;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('admin');
    }    


    public function index()
    {
        //
        if(Auth::user()->isAdminOrEditor()){
            $pages = Page::paginate(20);
        }else{
            $pages = Auth::user()->pages()->paginate(20);
        }
        return view('admin.pages.index', ['pages' => $pages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.pages.create', ['model' => new Page()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PagesManagement $request)
    {
        //
        Auth::user()->pages()->create($request->only(
            'title',
            'url',
            'content'
        ));

        return redirect()->route('pages.index')->with('status', 'Page is created');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        //
        if(Auth::user()->cant('update', $page)){
            return redirect()->route('pages.index')->with('status', 'You are not authorized to edit this page!');
        }
        return view('admin.pages.edit', ['model' => $page]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(PagesManagement $request, Page $page)
    {
        //
        if(Auth::user()->cant('update', $page)){
            return redirect()->route('pages.index')->with('status', 'You are not authorized to edit this page!');
        }

        $page->fill($request->only('title', 'url', 'content'));
        $page->save();

        return redirect()->route('pages.index')->with('status', 'Page edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        //
        if(Auth::user()->cant('delete', $page)){
            return redirect()->route('pages.index')->with('status', 'You are not authorized to delete this page!');
        }

        $page->delete();
        return redirect()->route('pages.index')->with('status', 'Page deleted');
    }
}
